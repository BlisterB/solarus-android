package org.solarus_games.solarus;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;


public class Solarus extends AppCompatActivity
        implements QuestFragment.OnListFragmentInteractionListener,
        QuestDetails.OnFragmentInteractionListener,
        PausedQuestScreen.OnFragmentInteractionListener,
        NavigationView.OnNavigationItemSelectedListener {

    private final int REQUEST_READ_STORAGE = 124;
    private final String TAG = "SolarusEngine";
    private final static String QUEST_DETAILS_DIALOG_TAG = "quest_dialog";
    private QuestFragment mQuestList;
    private DrawerLayout mDrawerLayout;
    private PausedQuestScreen mPausedQuestFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quest_launcher);

        boolean hasPermission = (ContextCompat.checkSelfPermission(getBaseContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(getBaseContext(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

        mQuestList = new QuestFragment();

        onNavigationItemSelected(R.id.nav_quests);
        setTitle("Quests"); //TODO find cleaner way

        if (!hasPermission) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_READ_STORAGE);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        // set item as selected to persist highlight
        menuItem.setChecked(true);
        // close drawer when item is tapped
        mDrawerLayout.closeDrawers();

        setTitle(menuItem.getTitle());

        // Add code here to update the UI based on the item selected
        // For example, swap UI fragments here
        return onNavigationItemSelected(menuItem.getItemId());
    }

    public boolean onNavigationItemSelected(int itemIndex) {
        Fragment fragment = mQuestList;
        switch(itemIndex) {
            case R.id.nav_settings:
                fragment = new SettingsFragment();
                break;
            case 2:
                //TODO about
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_layout, fragment)
                .commit();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case REQUEST_READ_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    finish();
                    startActivity(getIntent());
                    //reload my activity with permission granted or use the features what required the permission
                } else
                {
                    Toast.makeText(getBaseContext(), "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    ///List related methods
    @Override
    public void onQuestDetailClick(Quest quest) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        QuestDetails newDialog = QuestDetails.newInstance(quest.path);
        newDialog.show(ft,QUEST_DETAILS_DIALOG_TAG);
    }

    private void launchQuest(Quest quest) {
        Intent intent = new Intent(getApplicationContext(), SolarusEngine.class);
        if(quest.equals(SolarusApp.getCurrentQuest())) {
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        }
        Bundle b = new Bundle();
        b.putString("quest_path", quest.path);
        intent.putExtras(b);
        startActivity(intent);
    }

    @Override
    public void onQuestLaunchRequest(Quest quest) {
        if(SolarusApp.mustExitBeforeLaunch(quest)) {
            final Quest aquest = quest;

            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Another quest is running");
            alertDialog.setMessage("Quit current quest?");
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SolarusApp.finishCurrentEngine();
                            launchQuest(aquest);
                            dialog.dismiss();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //Do nothing...
                        }
                    });
            alertDialog.show();
        } else {
            launchQuest(quest);
        }
    }

    @Override
    public void onQuestQuitRequest(Quest q) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment dialog = getSupportFragmentManager().findFragmentByTag(QUEST_DETAILS_DIALOG_TAG);
        if(dialog != null) {
            ft.remove(dialog);
        }
        ft.commit();

        SolarusApp.finishCurrentEngine();
        mQuestList.notifyDataSetChanged(); //Notify that a quest has lost the "Now playing" status
        mPausedQuestFragment.update();
    }

    @Override
    public void onQuestResumeRequest(Quest quest) {
        Intent intent = new Intent(getApplicationContext(), SolarusEngine.class);
        if(quest.equals(SolarusApp.getCurrentQuest())) {
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        }
        Bundle b = new Bundle();
        b.putString("quest_path", quest.path);
        intent.putExtras(b);
        startActivity(intent);
    }

    @Override
    public void attachPausedQuestFragment(PausedQuestScreen f) {
        mPausedQuestFragment = f;
    }
}
