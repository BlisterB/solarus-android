package org.solarus_games.solarus;

import android.app.Application;
import android.content.Context;

public class SolarusApp extends Application {
    private static SolarusApp mInstance;
    private static SolarusEngine currentQuest;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static void setCurrentQuest(SolarusEngine a_quest) {
        currentQuest = a_quest;
    }

    public static Quest getCurrentQuest() {
        if(currentQuest != null) {
            return currentQuest.quest;
        } else {
            return null;
        }
    }

    public static SolarusEngine getCurrentEngine() {
        return currentQuest;
    }

    protected static void finishCurrentEngine() {
        if(currentQuest != null) {
            currentQuest.exit();
            currentQuest = null;
        }
    }

    protected static boolean nowPlaying(Quest quest) {
        return currentQuest != null && currentQuest.quest.equals(quest);
    }

    protected static boolean mustExitBeforeLaunch(Quest quest) {
        return currentQuest != null && !currentQuest.quest.equals(quest);
    }

    protected static void engineDown(SolarusEngine engine) {
        if(currentQuest == engine) {
            currentQuest = null;
        }
    }
    public static Context getContext() {
        return mInstance.getApplicationContext();
    }
}
